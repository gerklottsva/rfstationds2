#pragma once

#define WINVER 0x0A00
#define NOMINMAX 1
#include <iostream>
#include <queue>
#include <exception>
#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/lexical_cast.hpp>


#include "MyMsg.h"

class MyClient
{
public:
	MyClient(std::string host, std::string port, MyMsgQueue &in, MyMsgQueue& out, std::string defaultCmd, long timeout) : host(host), port(port), resolver_(io_context), socket_(io_context), timer_(io_context), in(in), out(out), stopped_(false), defaultCmd(defaultCmd), timeout_(timeout)
	{
        
	};

    ~MyClient()
    {
        cout << "Client deleted.." << endl;
    }
    void start()
    {
        cout << "Client started" << endl;
        //deadline_.async_wait(boost::bind(&MyClient::check_deadline, this));
        
        //deadline_.expires_after(boost::asio::chrono::seconds(10));  
        
        resolver_.async_resolve(boost::asio::ip::tcp::v4(), host, port,
            boost::bind(&MyClient::handle_resolve, this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::results));
        restartTimer();
        io_context.run();
        
    };

    void stop()
    {
        stopped_ = true;
        boost::system::error_code ignored_ec;
        socket_.close(ignored_ec);
        timer_.cancel();
    }

   /* void write_front()
    {
        cout << "write front" << endl;
        cout << in.size() << endl;

        if (in.empty())
        {
            cout << "empty" << endl;
            Sleep(1000);
            write_front();
        }
        else
        {

            MyMsg &msg = in.get();
           
            cout << msg.cmd << endl;
           /* if (msg.name == "sleep")
            {
                
                long t = std::stol(msg.arg);
                
                in.pop();
                
                Sleep(t);
                
                write_front();
                //cout << "we are here..." << endl;
            }
            */
           /* else if (msg.cmd.empty())
            {
                in.pop();
                write_front();
            }
            */
           // else
      /* {
                
                std::ostream rs(&request_);
                
                /*rs << msg.cmd;
                
                if (!msg.arg.empty())
                    rs << " " << msg.arg;
                rs << "\r\n";
                */
                /*
                cout << &request_ << std::endl;
                //deadline_.expires_after(boost::asio::chrono::seconds(5));
                
                boost::asio::async_write(socket_, request_,
                    boost::bind(&MyClient::handle_write_request, this,
                        boost::asio::placeholders::error));
                
                cout << "handle write request" << endl;
            }
        }
    }
    */

    /*void add_message(MyMsg msg)
    {
        in.add(msg);
    }
    */
private:
    void handle_resolve(const boost::system::error_code& err,
        const boost::asio::ip::tcp::resolver::results_type& endpoints)
    {
        if (err)
        {
            throw runtime_error(err.message());
        }
          
        // Attempt a connection to each endpoint in the list until we
        // successfully establish a connection.
        boost::asio::async_connect(socket_, endpoints,
            boost::bind(&MyClient::handle_connect, this,
                boost::asio::placeholders::error));
            
        
    }

    void handle_connect(const boost::system::error_code& err)
    {
        if (err)
        {
            throw runtime_error(err.message());
        }
        timer_.cancel();
        MyMsg stateMsg("dev_state", "state");
        stateMsg.reply = "on";
        out.add(stateMsg);
        MyMsg statusMsg("dev_status", "status");
        statusMsg.reply = "Socket connected!";
        out.add(statusMsg);
        writeMsg();
    }

    void on_timeout(const boost::system::error_code& e)
    {
        if (e != boost::asio::error::operation_aborted)
        {
            cout << "Timer expired.. Stop thread" << endl;
            stop();
            //socket_.close();
            // Timer was not cancelled, take necessary action.
        }
    }


    void handle_write_request(const boost::system::error_code& err)
    {
        if (err)
        {
            throw runtime_error(err.message());
        }
        
        //deadline_.async_wait(boost::bind(&MyClient::on_timeout, this, boost::asio::placeholders::error));

        boost::asio::async_read_until(socket_, response_, "\n",
            boost::bind(&MyClient::handle_read, this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::results));
        
    }

    void handle_read(const boost::system::error_code& err, std::size_t l)
    {
        //cout << "red: " << l << endl;
        timer_.cancel();
        if (err)
        {
            throw runtime_error(err.message());
        }
        
        if (in.empty())
        {
            Sleep(1000);
        }
        else
        {
            MyMsg &msg = in.get();
            std::stringstream ss;
            ss << &response_;
            msg.reply = ss.str();
            if (msg.reply.front() == '>')
                msg.reply = msg.reply.substr(1);
            //cout << msg.cmd << " " << msg.arg << " -> " << msg.reply << std::endl;
            out.add(msg);
            in.pop();
        }
        //write_front();
        writeMsg();
    }

    void writeMsg() {
        std::ostream rs(&request_);
        try
        {
            MyMsg& msg = in.get();
            if (msg.name == "sleep")
            {
                long t = std::stol(msg.arg);
                in.pop();
                Sleep(t);
                writeMsg();
                //cout << "we are here..." << endl;
            }
            else
            {
                rs << msg.cmd;
                if (!msg.arg.empty())
                    rs << " " << msg.arg;
                rs << "\r\n";
                restartTimer();
                boost::asio::async_write(socket_, request_,
                    boost::bind(&MyClient::handle_write_request, this,
                        boost::asio::placeholders::error));
            }
        }
        catch (...)
        {
            in.add(MyMsg("rio", "rio"));
            Sleep(1000);
            writeMsg();
        }
    }

    void timeout()
    {

        cout << "Timeout" << endl;
        stop();
    }

    void restartTimer()
    {
        timer_.expires_after(boost::asio::chrono::seconds(timeout_));
        timer_.async_wait(boost::bind(&MyClient::on_timeout, this, boost::asio::placeholders::error));
    }

    /*void check_deadline()
    {
        if (stopped_)
        {
            return;
        }

        // Check whether the deadline has passed. We compare the deadline against
        // the current time since a new asynchronous operation may have moved the
        // deadline before this actor had a chance to run.
        if (deadline_.expiry() <= boost::asio::steady_timer::clock_type::now())
        {
            
            // The deadline has passed. The socket is closed so that any outstanding
            // asynchronous operations are cancelled.
            socket_.close();
            
            // There is no longer an active deadline. The expiry is set to the
            // maximum time point so that the actor takes no action until a new
            // deadline is set.
            //deadline_.expires_at(boost::asio::steady_timer::time_point::max());
            deadline_.expires_after(boost::asio::chrono::seconds(10));   
        }
        
        // Put the actor back to sleep.
        deadline_.async_wait(boost::bind(&MyClient::check_deadline, this));
    }
    */

    

private:
    bool stopped_;
    std::string host;
    std::string port;
    std::string defaultCmd;
	boost::asio::io_context io_context;
	boost::asio::ip::tcp::resolver resolver_;
	boost::asio::ip::tcp::socket socket_;
    boost::asio::steady_timer timer_;
    boost::asio::streambuf request_;
    boost::asio::streambuf response_;
    MyMsgQueue& in, & out;
    long timeout_;
};

