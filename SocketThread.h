#pragma once
#include <string>
#include <tango.h>
#include <queue>
#include "MyClient.h"


namespace RFStationDS2_ns
{

class SocketThread : public omni_thread, public Tango::LogAdapter
{
public:
   /* SocketThread(TANGO_BASE_CLASS* dev, std::string host, std::string port, std::queue<MyMsg>& inMessages, std::queue<MyMsg>& outMessages, std::string defaultCmd) :
    omni_thread(),Tango::LogAdapter(dev), inMessages(inMessages), outMessages(outMessages), client(host, port, inMessages, outMessages, defaultCmd)
    */
    SocketThread(TANGO_BASE_CLASS* dev, std::string host, std::string port, MyMsgQueue &in, MyMsgQueue& out, std::string defaultCmd, long timeout) :
        omni_thread(), Tango::LogAdapter(dev), client(host, port, in, out, defaultCmd, timeout)
    {
        ds = dev;
		start_undetached();
    }
    ~SocketThread();
    void *run_undetached(void *);
    void stop();
 
private:
    TANGO_BASE_CLASS *ds;
    bool local_th_exit;
    MyClient client;
};
}