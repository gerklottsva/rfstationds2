/*----- PROTECTED REGION ID(RFStationDS2StateMachine.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        RFStationDS2StateMachine.cpp
//
// description : State machine file for the RFStationDS2 class
//
// project :     
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================

#include <RFStationDS2.h>

/*----- PROTECTED REGION END -----*/	//	RFStationDS2::RFStationDS2StateMachine.cpp

//================================================================
//  States   |  Description
//================================================================
//  ON       |  
//  FAULT    |  
//  RUNNING  |  


namespace RFStationDS2_ns
{
//=================================================
//		Attributes Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_Version_allowed()
 *	Description : Execution allowed for Version attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_Version_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for Version attribute in read access.
	/*----- PROTECTED REGION ID(RFStationDS2::VersionStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::VersionStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_Mod1Status_allowed()
 *	Description : Execution allowed for Mod1Status attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_Mod1Status_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for Mod1Status attribute in read access.
	/*----- PROTECTED REGION ID(RFStationDS2::Mod1StatusStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::Mod1StatusStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_Mod2Status_allowed()
 *	Description : Execution allowed for Mod2Status attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_Mod2Status_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for Mod2Status attribute in read access.
	/*----- PROTECTED REGION ID(RFStationDS2::Mod2StatusStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::Mod2StatusStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_Mod1Ready_allowed()
 *	Description : Execution allowed for Mod1Ready attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_Mod1Ready_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for Mod1Ready attribute in read access.
	/*----- PROTECTED REGION ID(RFStationDS2::Mod1ReadyStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::Mod1ReadyStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_Mod2Ready_allowed()
 *	Description : Execution allowed for Mod2Ready attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_Mod2Ready_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for Mod2Ready attribute in read access.
	/*----- PROTECTED REGION ID(RFStationDS2::Mod2ReadyStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::Mod2ReadyStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_ModLock_allowed()
 *	Description : Execution allowed for ModLock attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_ModLock_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for ModLock attribute in read access.
	/*----- PROTECTED REGION ID(RFStationDS2::ModLockStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::ModLockStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_RepeatCycle_allowed()
 *	Description : Execution allowed for RepeatCycle attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_RepeatCycle_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for RepeatCycle attribute in Write access.
	/*----- PROTECTED REGION ID(RFStationDS2::RepeatCycleStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::RepeatCycleStateAllowed_WRITE

	//	Not any excluded states for RepeatCycle attribute in read access.
	/*----- PROTECTED REGION ID(RFStationDS2::RepeatCycleStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::RepeatCycleStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_FastBufSize_allowed()
 *	Description : Execution allowed for FastBufSize attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_FastBufSize_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for FastBufSize attribute in read access.
	/*----- PROTECTED REGION ID(RFStationDS2::FastBufSizeStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::FastBufSizeStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_SlowBufSize_allowed()
 *	Description : Execution allowed for SlowBufSize attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_SlowBufSize_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for SlowBufSize attribute in read access.
	/*----- PROTECTED REGION ID(RFStationDS2::SlowBufSizeStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::SlowBufSizeStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_DblScalarRW_allowed()
 *	Description : Execution allowed for DblScalarRW attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_DblScalarRW_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for DblScalarRW attribute in Write access.
	/*----- PROTECTED REGION ID(RFStationDS2::DblScalarRWStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::DblScalarRWStateAllowed_WRITE

	//	Not any excluded states for DblScalarRW attribute in read access.
	/*----- PROTECTED REGION ID(RFStationDS2::DblScalarRWStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::DblScalarRWStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_Progress_allowed()
 *	Description : Execution allowed for Progress attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_Progress_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for Progress attribute in read access.
	/*----- PROTECTED REGION ID(RFStationDS2::ProgressStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::ProgressStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_DblSpectrum_allowed()
 *	Description : Execution allowed for DblSpectrum attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_DblSpectrum_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for DblSpectrum attribute in read access.
	/*----- PROTECTED REGION ID(RFStationDS2::DblSpectrumStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::DblSpectrumStateAllowed_READ
	return true;
}


//=================================================
//		Commands Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_Mod1On_allowed()
 *	Description : Execution allowed for Mod1On attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_Mod1On_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for Mod1On command.
	/*----- PROTECTED REGION ID(RFStationDS2::Mod1OnStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::Mod1OnStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_Mod1Off_allowed()
 *	Description : Execution allowed for Mod1Off attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_Mod1Off_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for Mod1Off command.
	/*----- PROTECTED REGION ID(RFStationDS2::Mod1OffStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::Mod1OffStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_Mod2On_allowed()
 *	Description : Execution allowed for Mod2On attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_Mod2On_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for Mod2On command.
	/*----- PROTECTED REGION ID(RFStationDS2::Mod2OnStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::Mod2OnStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_Mod2Off_allowed()
 *	Description : Execution allowed for Mod2Off attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_Mod2Off_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for Mod2Off command.
	/*----- PROTECTED REGION ID(RFStationDS2::Mod2OffStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::Mod2OffStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_Cycle_allowed()
 *	Description : Execution allowed for Cycle attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_Cycle_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::FAULT)
	{
	/*----- PROTECTED REGION ID(RFStationDS2::CycleStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::CycleStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_Save_allowed()
 *	Description : Execution allowed for Save attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_Save_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for Save command.
	/*----- PROTECTED REGION ID(RFStationDS2::SaveStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::SaveStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_Load_allowed()
 *	Description : Execution allowed for Load attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_Load_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for Load command.
	/*----- PROTECTED REGION ID(RFStationDS2::LoadStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::LoadStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_UpdateAttribute_allowed()
 *	Description : Execution allowed for UpdateAttribute attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_UpdateAttribute_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for UpdateAttribute command.
	/*----- PROTECTED REGION ID(RFStationDS2::UpdateAttributeStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::UpdateAttributeStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_ProcessData_allowed()
 *	Description : Execution allowed for ProcessData attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_ProcessData_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for ProcessData command.
	/*----- PROTECTED REGION ID(RFStationDS2::ProcessDataStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::ProcessDataStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_SetMode_allowed()
 *	Description : Execution allowed for SetMode attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_SetMode_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for SetMode command.
	/*----- PROTECTED REGION ID(RFStationDS2::SetModeStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::SetModeStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_SetS1UZazor_allowed()
 *	Description : Execution allowed for SetS1UZazor attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_SetS1UZazor_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for SetS1UZazor command.
	/*----- PROTECTED REGION ID(RFStationDS2::SetS1UZazorStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::SetS1UZazorStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_SetS2UZazor_allowed()
 *	Description : Execution allowed for SetS2UZazor attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_SetS2UZazor_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for SetS2UZazor command.
	/*----- PROTECTED REGION ID(RFStationDS2::SetS2UZazorStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::SetS2UZazorStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_SetS1IAnode_allowed()
 *	Description : Execution allowed for SetS1IAnode attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_SetS1IAnode_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for SetS1IAnode command.
	/*----- PROTECTED REGION ID(RFStationDS2::SetS1IAnodeStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::SetS1IAnodeStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_SetS2IAnode_allowed()
 *	Description : Execution allowed for SetS2IAnode attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_SetS2IAnode_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for SetS2IAnode command.
	/*----- PROTECTED REGION ID(RFStationDS2::SetS2IAnodeStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::SetS2IAnodeStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_SetDDSFrequency_allowed()
 *	Description : Execution allowed for SetDDSFrequency attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_SetDDSFrequency_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for SetDDSFrequency command.
	/*----- PROTECTED REGION ID(RFStationDS2::SetDDSFrequencyStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::SetDDSFrequencyStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_RebootCPU_allowed()
 *	Description : Execution allowed for RebootCPU attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_RebootCPU_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for RebootCPU command.
	/*----- PROTECTED REGION ID(RFStationDS2::RebootCPUStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::RebootCPUStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_Reconnect_allowed()
 *	Description : Execution allowed for Reconnect attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_Reconnect_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Compare device state with not allowed states.
	if (get_state()==Tango::ON ||
		get_state()==Tango::RUNNING)
	{
	/*----- PROTECTED REGION ID(RFStationDS2::ReconnectStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::ReconnectStateAllowed
		return false;
	}
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_UpdateCmd_allowed()
 *	Description : Execution allowed for UpdateCmd attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_UpdateCmd_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for UpdateCmd command.
	/*----- PROTECTED REGION ID(RFStationDS2::UpdateCmdStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::UpdateCmdStateAllowed
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : RFStationDS2::is_UploadTbl_allowed()
 *	Description : Execution allowed for UploadTbl attribute
 */
//--------------------------------------------------------
bool RFStationDS2::is_UploadTbl_allowed(TANGO_UNUSED(const CORBA::Any &any))
{
	//	Not any excluded states for UploadTbl command.
	/*----- PROTECTED REGION ID(RFStationDS2::UploadTblStateAllowed) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	RFStationDS2::UploadTblStateAllowed
	return true;
}


/*----- PROTECTED REGION ID(RFStationDS2::RFStationDS2StateAllowed.AdditionalMethods) ENABLED START -----*/

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	RFStationDS2::RFStationDS2StateAllowed.AdditionalMethods

}	//	End of namespace
