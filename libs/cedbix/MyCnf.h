#pragma once
//#ifndef MyCNF_H
//#define MyCNF_H

#include <string>
#include <sstream>
#include <map>
#include <exception>
#include <cctype>

namespace cedbix
{
	class MyCnf
	{
	public:

		MyCnf() {};
		MyCnf(std::string &cnfStr, char delim = ';')
		{
			std::stringstream cnf(cnfStr);
			std::string key = "";
			while (std::getline(cnf, key, '='))
			{
				std::string value = "";
				std::getline(cnf, value, delim);
				map_[key] = value;
			}
		}

		void Add(std::string keyValue)
		{
			std::stringstream cnf(keyValue);
			std::string key = "";
			std::string value = "";
			std::getline(cnf, key, '=');
			std::getline(cnf, value);
			if (!key.empty() && !value.empty())
				map_[trim(key)] = trim(value);
		}

		void Add(std::string key, std::string value)
		{
			map_[key] = value;
		}

		bool HasValue(std::string key) const
		{
			return map_.count(key) > 0;
		}

		std::string GetValue(std::string key) const
		{
			if (!HasValue(key))
				throw std::runtime_error(key + " not found");
			return map_.at(key);
		}

		std::string GetValueOrEmpty(std::string key) const
		{
			if (!HasValue(key))
				return "";
			return map_.at(key);
		}

		std::string GetValueOrException(std::string key, std::string desc) const
		{
			if (!HasValue(key))
				throw std::runtime_error(desc);
			return map_.at(key);
		}

		std::string GetValueOrDefault(std::string key, std::string def) const
		{
			if (!HasValue(key))
				return def;
			return map_.at(key);
		}

		void print() const
		{
			//for (auto &kv : map_)
			//	std::cout << kv.first << "=" << kv.second << std::endl;
		}

		std::vector<std::string> GetSplittedValue(std::string key, char delim = ',')
		{
			std::string value = GetValue(key);
			std::vector<std::string> res;
			std::stringstream cnf(value);
			std::string el = "";
			while (std::getline(cnf, el, delim))
			{
				res.push_back(trim(el));
			}
			return res;
		}

	private:

		std::string trim(const std::string& str)
		{
			size_t first = str.find_first_not_of(' ');
			if (std::string::npos == first)
			{
				return str;
			}
			size_t last = str.find_last_not_of(' ');
			return str.substr(first, (last - first + 1));
		}

		std::map<std::string, std::string> map_;
	};
}
//#endif   //	MyCNF_H
