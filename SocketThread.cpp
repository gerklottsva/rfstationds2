#include <tango.h>
#include <omnithread.h>
#include <log4tango.h>
#include <cmath>
#include "RFStationDS2.h"
#include "SocketThread.h"
#include "MyClient.h"


namespace RFStationDS2_ns
{

void * SocketThread::run_undetached(void *ptr)
{
    try
    {
        INFO_STREAM << "Socket thread starts..." << endl;
        //ds->set_state(Tango::ON);
        //ds->set_status("Socket running..");
        client.start();
        INFO_STREAM << "Socket thread stoped..." << endl;
    }
    catch (std::exception& e)
    {
        ERROR_STREAM << e.what() << endl;
        ds->set_state(Tango::FAULT);
        ds->set_status(e.what());
    }
    catch (...)
    {
        ERROR_STREAM << "Unknown error" << endl;
        ds->set_state(Tango::FAULT);
        ds->set_status("Unknown error");
    }
   	return 0;	
}

SocketThread::~SocketThread() {
}

void SocketThread::stop()
{
    INFO_STREAM << "Socket thread stopped..." << endl;
    client.stop();
}

} 