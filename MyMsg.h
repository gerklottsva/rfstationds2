#pragma once
#include <string>
#include <map>
#include <ctime>
#include <queue>
#include <tango.h>


class MyMsg
{
public:
    MyMsg(std::string name, std::string cmd, std::string arg = "", long index = 0) : name(name), cmd(cmd), arg(arg), index(index) {};
    std::string name;
    std::string cmd;
    std::string arg;
    long index;
    //time_t timestamp;
    std::string reply;
};

class MyMsgQueue
{
public:
    void add(MyMsg msg)
    {
        omni_mutex_lock lo(mutex);
        messages.push(msg);
    }

    MyMsg& get()
    {
        omni_mutex_lock lo(mutex);
        if (messages.empty())
            throw runtime_error("Queue is empty");
        return messages.front();
    }

    void pop()
    {
        omni_mutex_lock lo(mutex);
        if (!messages.empty())
            messages.pop();
    }

    bool empty()
    {
        omni_mutex_lock lo(mutex);
        return messages.empty();
    }

    size_t size()
    {
        omni_mutex_lock lo(mutex);
        return messages.size();
    }

private:
    std::queue<MyMsg> messages;
    omni_mutex mutex;
};
