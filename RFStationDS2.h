/*----- PROTECTED REGION ID(RFStationDS2.h) ENABLED START -----*/
//=============================================================================
//
// file :        RFStationDS2.h
//
// description : Include file for the RFStationDS2 class
//
// project :     
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef RFStationDS2_H
#define RFStationDS2_H

#include <tango.h>
#include <queue>
#include "MyMsg.h"
#include "SocketThread.h"


/*----- PROTECTED REGION END -----*/	//	RFStationDS2.h

/**
 *  RFStationDS2 class description:
 *    
 */

namespace RFStationDS2_ns
{
/*----- PROTECTED REGION ID(RFStationDS2::Additional Class Declarations) ENABLED START -----*/


//	Additional Class Declarations
	class MyAttributeInfo
	{
	public:
		std::string name;
		std::string cmd;
		int stationId;
		int bufferId;
		int index;
		std::string type;
	};

	class TblInfo
	{
	public:
		std::string name;
		int station;
		int table_id;
	};

/*----- PROTECTED REGION END -----*/	//	RFStationDS2::Additional Class Declarations

class RFStationDS2 : public TANGO_BASE_CLASS
{

/*----- PROTECTED REGION ID(RFStationDS2::Data Members) ENABLED START -----*/

//	Add your own data members
	/*std::queue<MyMsg> inMessages;
	std::queue<MyMsg> outMessages;
	*/
	MyMsgQueue controllerIn, controllerOut, testerIn, testerOut;
	//std::queue<MyMsg> testerInMessages;
	//std::queue<MyMsg> testerOutMessages;

	friend class SocketThread;
	SocketThread* controllerThread;
	SocketThread* testerThread;
	std::map<std::string, std::vector<double>> dblValues;
	std::map<std::string, MyAttributeInfo> attributeInfos;
	std::map<std::string, TblInfo> tableInfos;
	//int fastSize;
	//int slowSize;
	int fastStep;
	int slowStep;
	std::map<std::string, std::vector<std::string>> cmdAttributes;
	std::map<std::string, short> tblUploadProgresses;
	std::map<std::string, long> tblSizes;
	bool processing;

/*----- PROTECTED REGION END -----*/	//	RFStationDS2::Data Members

//	Device property data members
public:
	//	AttrConf:	
	vector<string>	attrConf;
	//	CmdConf:	
	vector<string>	cmdConf;
	//	UseTester:	
	Tango::DevBoolean	useTester;
	//	ControllerIP:	
	string	controllerIP;
	//	ControllerPort:	
	Tango::DevULong	controllerPort;
	//	TesterIP:	
	string	testerIP;
	//	TesterPort:	
	Tango::DevULong	testerPort;
	//	FastSize:	
	Tango::DevULong	fastSize;
	//	SlowSize:	
	Tango::DevULong	slowSize;
	//	TablesConf:	
	vector<string>	tablesConf;
	//	Timeout:	Seconds number to await controller reply
	Tango::DevLong	timeout;

//	Attribute data members
public:
	Tango::DevString	*attr_Version_read;
	Tango::DevBoolean	*attr_Mod1Status_read;
	Tango::DevBoolean	*attr_Mod2Status_read;
	Tango::DevBoolean	*attr_Mod1Ready_read;
	Tango::DevBoolean	*attr_Mod2Ready_read;
	Tango::DevBoolean	*attr_ModLock_read;
	Tango::DevBoolean	*attr_RepeatCycle_read;
	Tango::DevLong	*attr_FastBufSize_read;
	Tango::DevLong	*attr_SlowBufSize_read;

//	Constructors and destructors
public:
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	RFStationDS2(Tango::DeviceClass *cl,string &s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	RFStationDS2(Tango::DeviceClass *cl,const char *s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device name
	 *	@param d	Device description.
	 */
	RFStationDS2(Tango::DeviceClass *cl,const char *s,const char *d);
	/**
	 * The device object destructor.
	 */
	~RFStationDS2() {delete_device();};


//	Miscellaneous methods
public:
	/*
	 *	will be called at device destruction or at init command.
	 */
	void delete_device();
	/*
	 *	Initialize the device
	 */
	virtual void init_device();
	/*
	 *	Read the device properties from database
	 */
	void get_device_property();
	/*
	 *	Always executed method before execution command method.
	 */
	virtual void always_executed_hook();


//	Attribute methods
public:
	//--------------------------------------------------------
	/*
	 *	Method      : RFStationDS2::read_attr_hardware()
	 *	Description : Hardware acquisition for attributes.
	 */
	//--------------------------------------------------------
	virtual void read_attr_hardware(vector<long> &attr_list);
	//--------------------------------------------------------
	/*
	 *	Method      : RFStationDS2::write_attr_hardware()
	 *	Description : Hardware writing for attributes.
	 */
	//--------------------------------------------------------
	virtual void write_attr_hardware(vector<long> &attr_list);

/**
 *	Attribute Version related methods
 *	Description: 
 *
 *	Data type:	Tango::DevString
 *	Attr type:	Scalar
 */
	virtual void read_Version(Tango::Attribute &attr);
	virtual bool is_Version_allowed(Tango::AttReqType type);
/**
 *	Attribute Mod1Status related methods
 *	Description: 
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_Mod1Status(Tango::Attribute &attr);
	virtual bool is_Mod1Status_allowed(Tango::AttReqType type);
/**
 *	Attribute Mod2Status related methods
 *	Description: 
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_Mod2Status(Tango::Attribute &attr);
	virtual bool is_Mod2Status_allowed(Tango::AttReqType type);
/**
 *	Attribute Mod1Ready related methods
 *	Description: 
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_Mod1Ready(Tango::Attribute &attr);
	virtual bool is_Mod1Ready_allowed(Tango::AttReqType type);
/**
 *	Attribute Mod2Ready related methods
 *	Description: 
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_Mod2Ready(Tango::Attribute &attr);
	virtual bool is_Mod2Ready_allowed(Tango::AttReqType type);
/**
 *	Attribute ModLock related methods
 *	Description: 
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_ModLock(Tango::Attribute &attr);
	virtual bool is_ModLock_allowed(Tango::AttReqType type);
/**
 *	Attribute RepeatCycle related methods
 *	Description: 
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_RepeatCycle(Tango::Attribute &attr);
	virtual void write_RepeatCycle(Tango::WAttribute &attr);
	virtual bool is_RepeatCycle_allowed(Tango::AttReqType type);
/**
 *	Attribute FastBufSize related methods
 *	Description: 
 *
 *	Data type:	Tango::DevLong
 *	Attr type:	Scalar
 */
	virtual void read_FastBufSize(Tango::Attribute &attr);
	virtual bool is_FastBufSize_allowed(Tango::AttReqType type);
/**
 *	Attribute SlowBufSize related methods
 *	Description: 
 *
 *	Data type:	Tango::DevLong
 *	Attr type:	Scalar
 */
	virtual void read_SlowBufSize(Tango::Attribute &attr);
	virtual bool is_SlowBufSize_allowed(Tango::AttReqType type);

//	Dynamic attribute methods
public:

	/**
	 *	Attribute DblScalarRW related methods
	 *	Description: 
	 *
	 *	Data type:	Tango::DevDouble
	 *	Attr type:	Scalar
	 */
	virtual void read_DblScalarRW(Tango::Attribute &attr);
	virtual void write_DblScalarRW(Tango::WAttribute &attr);
	virtual bool is_DblScalarRW_allowed(Tango::AttReqType type);
	void add_DblScalarRW_dynamic_attribute(string attname);
	void remove_DblScalarRW_dynamic_attribute(string attname);
	Tango::DevDouble *get_DblScalarRW_data_ptr(string &name);
	map<string,Tango::DevDouble>	   DblScalarRW_data;

	/**
	 *	Attribute Progress related methods
	 *	Description: 
	 *
	 *	Data type:	Tango::DevShort
	 *	Attr type:	Scalar
	 */
	virtual void read_Progress(Tango::Attribute &attr);
	virtual bool is_Progress_allowed(Tango::AttReqType type);
	void add_Progress_dynamic_attribute(string attname);
	void remove_Progress_dynamic_attribute(string attname);
	Tango::DevShort *get_Progress_data_ptr(string &name);
	map<string,Tango::DevShort>	   Progress_data;

	/**
	 *	Attribute DblSpectrum related methods
	 *	Description: 
	 *
	 *	Data type:	Tango::DevDouble
	 *	Attr type:	Spectrum max = 10000
	 */
	virtual void read_DblSpectrum(Tango::Attribute &attr);
	virtual bool is_DblSpectrum_allowed(Tango::AttReqType type);
	void add_DblSpectrum_dynamic_attribute(string attname, Tango::DevDouble *ptr=NULL);
	void remove_DblSpectrum_dynamic_attribute(string attname, bool free_it=true);
	Tango::DevDouble *get_DblSpectrum_data_ptr(string &name);
	map<string,Tango::DevDouble *>	   DblSpectrum_data;

	//--------------------------------------------------------
	/**
	 *	Method      : RFStationDS2::add_dynamic_attributes()
	 *	Description : Add dynamic attributes if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_attributes();




//	Command related methods
public:
	/**
	 *	Command Mod1On related method
	 *	Description: 
	 *
	 */
	virtual void mod1_on();
	virtual bool is_Mod1On_allowed(const CORBA::Any &any);
	/**
	 *	Command Mod1Off related method
	 *	Description: 
	 *
	 */
	virtual void mod1_off();
	virtual bool is_Mod1Off_allowed(const CORBA::Any &any);
	/**
	 *	Command Mod2On related method
	 *	Description: 
	 *
	 */
	virtual void mod2_on();
	virtual bool is_Mod2On_allowed(const CORBA::Any &any);
	/**
	 *	Command Mod2Off related method
	 *	Description: 
	 *
	 */
	virtual void mod2_off();
	virtual bool is_Mod2Off_allowed(const CORBA::Any &any);
	/**
	 *	Command Cycle related method
	 *	Description: 
	 *
	 */
	virtual void cycle();
	virtual bool is_Cycle_allowed(const CORBA::Any &any);
	/**
	 *	Command Save related method
	 *	Description: 
	 *
	 *	@param argin S1U1, S1U2, S1F1, S1F2, S1IA, S2U1, S2U2, S2F1, S2F2, S2IA
	 */
	virtual void save(Tango::DevString argin);
	virtual bool is_Save_allowed(const CORBA::Any &any);
	/**
	 *	Command Load related method
	 *	Description: 
	 *
	 */
	virtual void load();
	virtual bool is_Load_allowed(const CORBA::Any &any);
	/**
	 *	Command UpdateAttribute related method
	 *	Description: 
	 *
	 *	@param argin 1 - AttributeName
	 *               2 - PointsNumber
	 */
	virtual void update_attribute(const Tango::DevVarStringArray *argin);
	virtual bool is_UpdateAttribute_allowed(const CORBA::Any &any);
	/**
	 *	Command ProcessData related method
	 *	Description: 
	 *
	 */
	virtual void process_data();
	virtual bool is_ProcessData_allowed(const CORBA::Any &any);
	/**
	 *	Command SetMode related method
	 *	Description: 
	 *
	 *	@param argin 
	 */
	virtual void set_mode(Tango::DevString argin);
	virtual bool is_SetMode_allowed(const CORBA::Any &any);
	/**
	 *	Command SetS1UZazor related method
	 *	Description: 
	 *
	 *	@param argin 
	 */
	virtual void set_s1_uzazor(Tango::DevFloat argin);
	virtual bool is_SetS1UZazor_allowed(const CORBA::Any &any);
	/**
	 *	Command SetS2UZazor related method
	 *	Description: 
	 *
	 *	@param argin 
	 */
	virtual void set_s2_uzazor(Tango::DevFloat argin);
	virtual bool is_SetS2UZazor_allowed(const CORBA::Any &any);
	/**
	 *	Command SetS1IAnode related method
	 *	Description: 
	 *
	 *	@param argin 
	 */
	virtual void set_s1_ianode(Tango::DevFloat argin);
	virtual bool is_SetS1IAnode_allowed(const CORBA::Any &any);
	/**
	 *	Command SetS2IAnode related method
	 *	Description: 
	 *
	 *	@param argin 
	 */
	virtual void set_s2_ianode(Tango::DevFloat argin);
	virtual bool is_SetS2IAnode_allowed(const CORBA::Any &any);
	/**
	 *	Command SetDDSFrequency related method
	 *	Description: 
	 *
	 *	@param argin 
	 */
	virtual void set_ddsfrequency(Tango::DevFloat argin);
	virtual bool is_SetDDSFrequency_allowed(const CORBA::Any &any);
	/**
	 *	Command RebootCPU related method
	 *	Description: 
	 *
	 */
	virtual void reboot_cpu();
	virtual bool is_RebootCPU_allowed(const CORBA::Any &any);
	/**
	 *	Command Reconnect related method
	 *	Description: 
	 *
	 */
	virtual void reconnect();
	virtual bool is_Reconnect_allowed(const CORBA::Any &any);

//	Dynamic commands methods
public:
	/**
	 *	Command UpdateCmd related method
	 *	Description: 
	 *
	 */
	virtual void update_cmd(Tango::Command &command);
	virtual bool is_UpdateCmd_allowed(const CORBA::Any &any);
	void add_UpdateCmd_dynamic_command(string cmdname, bool device);
	void remove_UpdateCmd_dynamic_command(string cmdname);
	/**
	 *	Command UploadTbl related method
	 *	Description: 
	 *
	 *	@param argin 
	 */
	virtual void upload_tbl(const Tango::DevVarDoubleArray *argin,Tango::Command &command);
	virtual bool is_UploadTbl_allowed(const CORBA::Any &any);
	void add_UploadTbl_dynamic_command(string cmdname, bool device);
	void remove_UploadTbl_dynamic_command(string cmdname);

	//--------------------------------------------------------
	/**
	 *	Method      : RFStationDS2::add_dynamic_commands()
	 *	Description : Add dynamic commands if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_commands();

/*----- PROTECTED REGION ID(RFStationDS2::Additional Method prototypes) ENABLED START -----*/

//	Additional Method prototypes

	void updateAttributes(std::vector<std::string>& attNames, long pointNumber = 0);

/*----- PROTECTED REGION END -----*/	//	RFStationDS2::Additional Method prototypes
};

/*----- PROTECTED REGION ID(RFStationDS2::Additional Classes Definitions) ENABLED START -----*/

//	Additional Classes Definitions

/*----- PROTECTED REGION END -----*/	//	RFStationDS2::Additional Classes Definitions

}	//	End of namespace

#endif   //	RFStationDS2_H
